# Build espmonitor first
FROM docker.io/library/rust:alpine AS BUILDER

WORKDIR /root
RUN apk add --no-cache musl-dev
RUN cargo install espmonitor

# This creates the actual finished container
FROM docker.io/alpine:latest

ENV NAME=espflash ARCH=x86_64
LABEL   name="$NAME" \
        architecture="$ARCH" \
        run="podman run -v /dev:/dev -v $PWD:/project:z -i --security-opt label=disable IMAGE ..." \
        summary="Monitor the output of an esp MCU." \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/espmonitor"

# Move espmonitor to a new container
COPY --from=BUILDER /usr/local/cargo/bin/espmonitor /usr/bin/espmonitor

# Copy documentation into container
COPY help.md /

VOLUME /project
WORKDIR /project

ENTRYPOINT [ "/usr/bin/espmonitor" ]
