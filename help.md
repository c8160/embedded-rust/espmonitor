espmonitor image for running espmonitor from a container.

Packages the [espmonitor utility](https://crates.io/crates/espmonitor).

Volumes:
PWD: Mapped to /project in the container, used to exchange files with the host.
  Without this, the binary cannot map HEX codes in the output to function names
  in the binary.  Note that if you mount your $PWD, the binary to flash must be
  accessible from within or below the $PWD. Relative paths starting with ".."
  aren't allowed.
/dev: Mapped to /dev in the container, required to communicate with the ESP MCU
  to flash the software to.

Documentation:
For the underlying espmonitor project, see https://github.com/esp-rs/espmonitor
For this container, see https://gitlab.com/c8160/embedded-rust/espmonitor

Requirements:
Needs an ESP device to monitor serial output from. Most important, this device
must be read/writable by the user executing the container. You can either
achieve this by `chown`ing or `chmod`ing the respective entry in
`/dev/ttyUSB*`. Or, for a more permanent solution, create a udev rule to do
this automatically for you.
